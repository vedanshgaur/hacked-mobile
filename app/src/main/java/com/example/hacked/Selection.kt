package com.example.hacked

import java.util.Random;

class Selection {
    var selections = arrayOf("Yes", "No", "Maybe")

    fun select(): String {
        val randomNum = Random().nextInt(3)
        return selections[randomNum]
    }
}

// for testing
//fun main() {
//    val myselection = selection().select()
//    println(myselection)
//}
